import 'package:chat_app/domain/entities/message.entity.dart';
import 'package:chat_app/infrastructure/models/yesno.model.dart';
import 'package:dio/dio.dart';

class GetYesNoApi {
  final _dio = Dio();

  Future<Message> getMessage() async {
    final response = await _dio.get("https://yesno.wtf/api");
    final yesNoModel = YesNoModel.fromJson(response.data);
    return yesNoModel.toMessageEntity();
  }
}
