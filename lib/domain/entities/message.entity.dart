class Message {
  final String msg;
  final String? imgUrl;
  final bool receiver;
  final bool read;

  Message(
      {required this.msg,
      this.imgUrl,
      required this.receiver,
      required this.read});
}
