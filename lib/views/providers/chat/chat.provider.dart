import 'package:chat_app/config/helpers/get_yesno_api.helper.dart';
import 'package:chat_app/domain/entities/message.entity.dart';
import 'package:flutter/material.dart';

class ChatProvider extends ChangeNotifier {
  final chatScrollController = ScrollController();
  final getYesNoApi = GetYesNoApi();

  List<Message> messageList = [
    Message(
      msg: "Hola como estas?",
      receiver: false,
      read: true,
    )
  ];

  Future<void> sendMessage({required String msg}) async {
    if (msg.isEmpty) return;
    final newMessage = Message(msg: msg, receiver: false, read: false);
    messageList.add(newMessage);

    if (msg.endsWith("?")) {
      await answerReply();
    }

    notifyListeners();
    moveScrollToBottom();
  }

  Future<void> answerReply() async {
    final msg = await getYesNoApi.getMessage();
    messageList.add(msg);
  }

  Future<void> moveScrollToBottom() async {
    await Future.delayed(const Duration(milliseconds: 200));

    chatScrollController.animateTo(
      chatScrollController.position.maxScrollExtent,
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeInOut,
    );
  }
}
