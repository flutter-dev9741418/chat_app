import 'package:chat_app/views/providers/chat/chat.provider.dart';
import 'package:chat_app/views/widgets/chat/her_message_bubble.widget.dart';
import 'package:chat_app/views/widgets/shared/message_field.widget.dart';
import 'package:flutter/material.dart';

import 'package:chat_app/views/widgets/chat/my_message_bubble.widget.dart';
import 'package:provider/provider.dart';

class ChatScreen extends StatelessWidget {
  const ChatScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const Padding(
          padding: EdgeInsets.all(8.0),
          child: CircleAvatar(
            backgroundImage: NetworkImage(
                'https://pbs.twimg.com/profile_images/759477117616873472/iEkP1rai_400x400.jpg'),
          ),
        ),
        title: const Text("× Yendy ×"),
        centerTitle: false,
      ),
      body: _ChatView(),
    );
  }
}

class _ChatView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final chatProvider = context.watch<ChatProvider>();

    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                controller: chatProvider.chatScrollController,
                itemCount: chatProvider.messageList.length,
                itemBuilder: (context, index) {
                  final msg = chatProvider.messageList[index];

                  return msg.receiver
                      ? HerMessageBubbleWidget(
                          msg: msg,
                        )
                      : MyMessageBubbleWidget(
                          msg: msg,
                        );
                },
              ),
            ),
            MessageFieldWidget(
              onValue: (value) => chatProvider.sendMessage(msg: value),
            ),
          ],
        ),
      ),
    );
  }
}
