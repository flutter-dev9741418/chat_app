import 'package:chat_app/domain/entities/message.entity.dart';
import 'package:flutter/material.dart';

class HerMessageBubbleWidget extends StatelessWidget {
  final Message msg;
  const HerMessageBubbleWidget({super.key, required this.msg});

  @override
  Widget build(BuildContext context) {
    final colors = Theme.of(context).colorScheme;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          decoration: BoxDecoration(
              color: colors.secondary, borderRadius: BorderRadius.circular(8)),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Text(
              msg.msg,
              style: const TextStyle(color: Colors.white),
            ),
          ),
        ),
        const SizedBox(height: 10),
        _ImagenBubble(imgUrl: msg.imgUrl!),
        const SizedBox(height: 10),
      ],
    );
  }
}

class _ImagenBubble extends StatelessWidget {
  final String imgUrl;

  const _ImagenBubble({required this.imgUrl});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return ClipRRect(
      borderRadius: BorderRadius.circular(8),
      child: Image.network(
        imgUrl,
        width: size.width * 0.7,
        height: 150,
        fit: BoxFit.cover,
        loadingBuilder: (context, child, loadingProgress) {
          if (loadingProgress == null) return child;

          return Container(
            width: size.width * 0.7,
            height: 150,
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: const Text("× Yendy × esta enviando una imagen."),
          );
        },
      ),
    );
  }
}
