import 'package:chat_app/views/providers/chat/chat.provider.dart';
import 'package:flutter/material.dart';

import 'package:chat_app/config/themes/app.theme.dart';
import 'package:chat_app/views/screens/chat/chat.screen.dart';
import 'package:provider/provider.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ChatProvider()),
      ],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Material App',
          theme: AppTheme(selectColor: 1, mode: Brightness.light).theme(),
          home: const ChatScreen()),
    );
  }
}
